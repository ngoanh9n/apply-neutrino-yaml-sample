### PREREQUISITES
1. Java JDK: https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
2. Allure: https://github.com/allure-framework/allure2/releases
3. ANSI Colors Handling on Windows: https://superuser.com/questions/413073/windows-console-with-ansi-colors-handling

### EXECUTIONS:
#### Commands:
```java
<java -jar neutrino-yaml.jar run --browser chrome>
<java -jar neutrino-yaml.jar run --browser firefox>

// Run with headless mode
<java -jar neutrino-yaml.jar run --browser chrome --headless>
<java -jar neutrino-yaml.jar run --browser firefox --headless>
```

#### See reports:
```java
<allure serve reports>
```

### COMBINED ACTION, MODULE, SCRIPT USAGE:
1. Script able to contains: Primitive actions, combined actions, modules
2. Combined action able to contains: Primitive actions, modules
3. Module able to contains: Verifying for element type only

#### ACTION USAGE:
_Locator: Includes a how-using pair which defined in pages/components (repositories)_
##### Attack:
##### Attack - Remove Attribute:
```yml
attack:
  element: "Name of locator"
  remove:
    attribute-name: "Name of attribute"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Attack - Set (Add/Update) Attribute:
```yml
attack:
  element: "Name of locator"
  set:
    attribute-name: "Name of attribute"
    attribute-value: "Value of attribute"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Clear:
```yml
clear:
  element: "Name of locator"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Click:
```yml
click:
  element: "Name of locator"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

```yml
click:
  byText: "VisibleText"
  alias: "Set alias for text element"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Double Click:
```yml
doubleClick:
  element: "Name of locator"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

```yml
doubleClick:
  byText: "VisibleText"
  alias: "Set alias for text element"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Hover:
```yml
hover:
  element: "Name of locator"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)
```yml
hover:
  byText: "VisibleText"
  alias: "Set alias for text element"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Input:
```yml
input:
  element: "Name of locator"
  value: "value"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Right Click:
```yml
rightClick:
  element: "Name of locator"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

```yml
rightClick:
  byText: "VisibleText"
  alias: "Set alias for text element"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Scroll:
```yml
scrollInto:
  element: "Name of locator"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

```yml
scroll:
  byText: "VisibleText"
  alias: "Set alias for text element"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Select:
```yml
select:
  element: "Name of locator"
  byIndex: "index"
```
```yml
select:
  element: "Name of locator"
  byText: "VisibleTextOfOption"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Verify:
##### Verify - Status:
```yml
verify:
  element: "Name of locator"
  status: "OPTIONS"

# OPTIONS: present/absent/visible/invisible/selected/unselected/enable/disable
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Verify - Status (Elements):
```yml
verify:
  elements: "Name of locator as a collection"
  status: "OPTIONS"

# OPTIONS: visible/invisible/selected/unselected/enable/disable
```
Command's options: As a child of this step
[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Verify - Visible Text:
```yml
verify:
  element: "Name of locator"
  visibleText:
    assert: "OPTIONS"
    value: "value"

# OPTIONS: equals/notEquals/contains/notContains/matches
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Verify - Attribute:
```yml
verify:
  element: "Name of locator"
  attribute:
    name: "Name of attribute"
    assert: "OPTIONS"
    value: "value"
    alias: "Alias of this attribute if necessary"

# OPTIONS: equals/notEquals/contains/notContains/matches
```
```yml
verify:
  element: "Name of locator"
  attribute:
    name: "Name of attribute"
    existed: "OPTIONS"

# OPTIONS: true/false
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Verify - CSS:
```yml
verify:
  element: "Name of locator"
  css:
    name: "Name of css"
    assert: "OPTIONS"
    value: "value"
    alias: "Alias of this css if necessary"

# OPTIONS: equals/notEquals/contains/notContains
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Verify - CSS (Elements):
```yml
verify:
  elements: "Name of locator as a collection"
  css:
     name: "Name of css"
     assert: "OPTIONS"
     value: "value"

# OPTIONS: equals/notEquals/contains/notContains
```
Command's options: As a child of this step
[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Verify - ComboBox/DropdownBox Options:
```yml
verify:
  element: "Name of locator"
  options:
    check: "OPTIONS"
    value: "Visible Text Of Option"

# OPTIONS: selectedOption/hasOption/hasOptionsNumber
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Verify - Siblings:
```yml
verify:
  element: "Name of locator as a collection"
  siblings:
    assert: "OPTIONS"
    value: "value"

# OPTIONS: equals/notEquals/lessThan/moreThan
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Wait - Status:
```yml
wait:
  element: "Name of locator"
  until: "OPTIONS"

# OPTIONS: visible/invisible/selected/unselected/enable/disable
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Wait - Attribute:
```yml
wait:
  element: "Name of locator"
  attribute:
    name: "Name of attribute"
    assert: "OPTIONS"
    value: "value"

# OPTIONS: equals/notEquals/contains/notContains
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Open:
```yml
open:
  link: "http://shiftasia.com"
  alias: "Alias of page"
```
Command's options: As a child of this step

[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Text:
```yml
text:
  element: "Name of locator"
  assign: "#{YourVariableName}"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Count:
```yml
count:
  element: "Name of locator as a collection"
  assign: "#{YourVariableName}"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Child](#childs-use-case) /
[Sibling](#siblings-use-case)

##### Press:
```yml
press:
  key: "Name of Key"
```
```yml
press:
  key: "Name of Key"
  times: "number"
```
Command's options: As a child of this step

[Sleep](#sleep) /
[Attach](#attachs-use-case) /
[Capture](#captures-use-case)

##### Sleep:
```yml
sleep:
  seconds: "number"
```

##### Assign:
```yml
assign:
  from: "#{YourVar1}"
  to: "#{YourVar2}"

# Assign value of 'YourVar1' to 'YourVar2'
```

##### Generate:
```yml
generate:
  count: "number"
  letters: "true/false"
  numbers: "true/false"
  prefix: "text"
  suffix: "text"
  assign: "#{YourVar}"
```

##### Print:
```yml
print:
  value: "YourValue / #{YourAssigned} / ${YourDataName} / $${YourCollection}$"
```

##### Statement:
```yml
statement:
  when:
    [Use-Cases]
  then:
    [Steps]
  else:
    [Steps]
```
```yml
statement:
  while:
    [Use-Cases]
  then:
    [Steps]
```
[When/While's Use-Case](#whenwhiles-use-case)
```yml
statement:
  foreach:
    [Use-Cases]
  then:
    [Steps]
```
[Foreach's Use-Case](#foreachs-use-case)

#### Attach's Use-Case
```yml
attach:
  message: "The content of message, shown at step involved in allure reports"
```

#### Capture's Use-Case
```yml
capture:
  filename: "Name of PNG file"
  category: "Make a folder for running scenario"
  always: "true"
  comparesWith: "Name of expected image captured before"

# [always: "true"]: This step is captured always even though animation disabled
# [comparesWith: "Name of Expected"]: Meaning compares between image of this step and image of previous step matches by filename
# Captured files are stored in ../resources/screenshots/{category}
```

#### Child's Use-Case
_Locate by a child when root element as a wrapper/container_
```yml
child:
  element: "Name of locator"
  index: "index"
  alias: "Set alias for this sibling element if necessary"
  #...
  child:
    #...
    child:
        #...
            # So On

# Use [element: "Name of locator"] when locator is defined in a repository
# Use [index: "index"] when [element: "Name of locator"] as a collection (siblings)
```
```yml
child:
  how: "how"
  using: "using"
  index: "index"
  alias: "Set alias for this sibling element if necessary"
  #...
  child:
    #...
    child:
        #...
            # So On

# Use [how-using] pair when locator had not defined in any repository
# Use [index: "index"] when [how-using] pair as a collection (siblings)
```

#### Sibling's Use-Case
_Locate by attributes when root element as a collection_
```yml
sibling:
  - attribute: "Name of attribute"
    value: "Value of attribute"
    alias: "Set alias for this sibling element if necessary"
  - attribute: "So on if locate by multi pairs"

# Use 'sibling' when getting a sibling in list by attributes
```

#### When/While's Use-Case
##### When/While's Use-Case - String Comparison
```yml
text:
    operand: "from-value"
    assert: "OPTIONS"
    value: "to-value"

# OPTIONS: equals/notEquals/contains/notContains/matches/notMatches
```

##### When/While's Use-Case - Number Comparison
```yml
number:
    operand: "from-value"
    assert: "OPTIONS"
    value: "to-value"

# OPTIONS: equals/notEquals/lessThan/moreThan/lessThanEquals/moreThanEquals
```

##### When/While's Use-Case - Status Condition
```yml
element: "Name of locator"
status: "OPTIONS"

# OPTIONS: visible/invisible/selected/unselected/enable/disable
```
```yml
element: "Name of locator as a wrapper"
child: "Child's Use-Case"
status: "OPTIONS"

# OPTIONS: visible/invisible/selected/unselected/enable/disable
```
```yml
element: "Name of locator as a collection"
sibling:
   - attribute: "Name of attribute"
     value: "Value of attribute"
     alias: "Set alias for this sibling element if necessary"
status: "OPTIONS"

# OPTIONS: visible/invisible/selected/unselected/enable/disable
# Use 'sibling' when getting a sibling in list by attributes
```

##### When/While's Use-Case - Visible Text Condition
```yml
element: "Name of locator"
visibleText:
  assert: "OPTIONS"
  value: "value"

# OPTIONS: equals/notEquals/contains/notContains
```
```yml
element: "Name of locator as a wrapper"
child: "Child's Use-Case"
visibleText:
  assert: "OPTIONS"
  value: "value"

# OPTIONS: equals/notEquals/contains/notContains
```
```yml
element: "Name of locator as a collection"
sibling:
   - attribute: "Name of attribute"
     value: "Value of attribute"
     alias: "Set alias for this sibling element if necessary"
visibleText:
  assert: "OPTIONS"
  value: "value"

# OPTIONS: equals/notEquals/contains/notContains
# Use 'sibling' when getting a sibling in list by attributes
```

##### When/While's Use-Case - Attribute Condition
```yml
element: "Name of locator"
attribute:
  name: "Name of attribute"
  assert: "OPTIONS"
  value: "value"

# OPTIONS: equals/notEquals/contains/notContains
```
```yml
element: "Name of locator as a wrapper"
child: "Child's Use-Case"
attribute:
    name: "Name of attribute"
    assert: "OPTIONS"
    value: "value"

# OPTIONS: equals/notEquals/contains/notContains
```
```yml
element: "Name of locator as a collection"
sibling:
   - attribute: "Name of attribute"
     value: "Value of attribute"
     alias: "Set alias for this sibling element if necessary"
attribute:
    name: "Name of attribute"
    assert: "OPTIONS"
    value: "value"

# OPTIONS: equals/notEquals/contains/notContains
# Use 'sibling' when getting a sibling in list by attributes
```

##### When/While's Use-Case - Number Of Siblings Condition
```yml
element: "Name of locator as a collection"
siblings:
    assert: "OPTIONS"
    value: "value"

# OPTIONS: equals/notEquals/moreThan/lessThan
```

#### Foreach's Use-Case
```yml
collection: "$${YourCollectionName}"

# Use "$${YourCollectionName}$" to receive value from ForEach
```
```yml
element: "Name of locator Group"
visibleText:
  assign: "#{YourVariableName} [ref-#]"

# Use #YourVariableName to receive value from Assign
```
```yml
element: "Name of locator Group"
attribute:
  name: "Name of attribute"
  assign: "#{YourVariableName} [ref-#]"

# Use #YourVariableName to receive value from Assign
```
```yml
range: "&{fromNumber..toNumber}"

# Use "&{fromNumber..toNumber}.index" to get current index
```

### HOWS:
[Hows guide](../blob/master/guides/hows.md)

### KEYS:
[Keys guide](../blob/master/guides/keys.md)

### CURSORS:
[Cursors guide](../blob/master/guides/cursors.md)

### REGEX:
[Regex guide](../blob/master/guides/regex.md)